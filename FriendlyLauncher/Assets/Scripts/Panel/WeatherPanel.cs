﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeatherPanel : MonoBehaviour
{
	public Text CityCountry;
	public Text Coordinate;
	public Text WeatherInfo;
	public Image WeatherIcon;
	public Text Temp;
	public Text MinMaxTemp;
	public Text FeelsLikeTemp;
	public Text Humidity;

	private void Update()
	{
		if (string.IsNullOrEmpty(Manager.GeoManager.CurrentData.WeatherCity)) return;
		WeatherData temp = Manager.GeoManager.CurrentData;
		CityCountry.text = $"{temp.WeatherCity} - {temp.Country}";

		Coordinate.text = $"{Mathf.Abs(temp.Coordinate.x):F1}°{(temp.Coordinate.x > 0 ? "N" : "S")}, {Mathf.Abs(temp.Coordinate.y):F1}°{(temp.Coordinate.y > 0 ? "E" : "W")}";
		WeatherInfo.text = temp.WeatherInfo;
		WeatherIcon.sprite = temp.WeatherIcon;

		TempUnit choice = Manager.GeoManager.TempUnit;
		string FC = choice == TempUnit.Celsius ? "C" : "F";

		Temp.text = $"{temp.Temp(choice):F1}°{FC}";
		MinMaxTemp.text = $"Min Max temp:\n {temp.MinMaxTemp(choice).x:F1}°{FC} ~ {temp.MinMaxTemp(choice).y:F1}°{FC}";
		FeelsLikeTemp.text = $"Feels Like temp:\n{temp.FeelsLikeTemp(choice):F1}°{FC}";
		Humidity.text = $"Humidity:\n {temp.Humidity:F1}%";
	}

	public void RefreshGeoData()
	{
		Manager.GeoManager.UpdateWeatherData();
	}

	public void ClosePanel()
	{
		this.gameObject.SetActive(false);
	}
}
