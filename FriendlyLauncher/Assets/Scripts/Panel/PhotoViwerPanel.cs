﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoViwerPanel : MonoBehaviour
{
	public RawImage PhotoRaw;

	public void ClosePanel()
	{
		this.gameObject.SetActive(false);
	}

	public void LoadPhoto(Texture texture)
	{
		PhotoRaw.GetComponent<AspectRatioFitter>().aspectRatio = texture.width / texture.height;
		PhotoRaw.texture = texture;
	}
}
