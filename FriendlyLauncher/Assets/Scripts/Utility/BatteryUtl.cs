﻿using JacobGames.SuperInvoke;
using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryUtl : MonoBehaviour
{

    public ProgressBar BatteryBar;
    // Start is called before the first frame update
    void Start()
    {
        SuperInvoke.RunRepeat(0, 1, SuperInvoke.INFINITY, delegate { BatteryBar.currentPercent = Mathf.Lerp(0, 100, SystemInfo.batteryLevel); });
    }
}
