﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeatherUtl : MonoBehaviour
{
    public Text WeatherCity;
    public Image WeatherIcon;
    public Text Temperature;

    // Start is called before the first frame update
    void Update()
    {
        if (string.IsNullOrEmpty(Manager.GeoManager.CurrentData.WeatherInfo)) return;
        WeatherData temp = Manager.GeoManager.CurrentData;
        TempUnit choice = Manager.GeoManager.TempUnit;
        string FC = choice == TempUnit.Celsius ? "C" : "F";

        WeatherCity.text = temp.WeatherCity;
        WeatherIcon.sprite = temp.WeatherIcon;
        Temperature.text = $"{temp.Temp(choice):F1}°{FC}";
    }

     
}
