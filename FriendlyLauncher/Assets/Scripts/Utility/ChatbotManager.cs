﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.Text;
using UnityEngine.UI;
using UnityEngine.Networking;
using com.ootii.Messages;

public class ChatbotManager : MonoBehaviour
{
	private string url = "http://openapi.tuling123.com/openapi/api/v2";

	private string apiKey = "9646656a43064fb0acada6a7a411c3e1";
	public string useriId;
	Coroutine SendToBotRountine;


	private void Awake()
	{
		MessageDispatcher.AddListener("SendToChatBot",
		msg =>
		{
			string TextMsg = (string)msg.Data;
			SendToBot(TextMsg);
		},
		true);
	}

	void SendToBot(string message)
	{
		if (SendToBotRountine != null) StopCoroutine(SendToBotRountine);
		StartCoroutine(SendToBotTask(message));
	}

	IEnumerator SendToBotTask(string message)
	{
		//JsonData可以表示JsonObject{},也可以表示JsonArry[]//4e03ee9c4e8cc2af//"1512267543"
		JsonData request = new JsonData();
		//perception
		request["perception"] = new JsonData();
		request["perception"]["inputText"] = new JsonData();
		request["perception"]["inputText"]["text"] = message;
		//userInfo
		request["userInfo"] = new JsonData();
		request["userInfo"]["apiKey"] = apiKey;
		request["userInfo"]["userId"] = useriId;

		//JsonMapper.ToJson(request)
		//将Json对象转为Json字符串,直接ToString容易出错
		//将Json字符串转为字节数组
		//进行一个网络推送


		var www = new UnityWebRequest(url, "POST");
		byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonMapper.ToJson(request));
		www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
		www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
		www.SetRequestHeader("Content-Type", "application/json");
		yield return www.SendWebRequest();
		//Debug.Log("Status Code: " + www.responseCode);

		JsonData response = JsonMapper.ToObject(www.downloadHandler.text);

		//Debug.Log(www.downloadHandler.text);
		string result = response["results"][0]["values"]["text"].ToString();
		MessageDispatcher.SendMessage(this, "ChatBotReply", result, 0);
	}
}