﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Networking;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using com.ootii.Messages;
using System.Linq;
using SimpleJSON;

[System.Serializable]
public struct WeatherData
{
	public Sprite WeatherIcon;
	public string WeatherInfo;
	public string WeatherCity;
	public string Country;
	public Vector2 Coordinate;
	public float Humidity;

	public float Temp(TempUnit input)
	{
		return input == TempUnit.Fahrenheit ? FahrenheitTemp : CelsiusTemp;
	}

	public Vector2 MinMaxTemp(TempUnit input)
	{
		return input == TempUnit.Fahrenheit ? MinMaxFahrenheitTemp : MinMaxCelsiusTemp;
	}

	public float FeelsLikeTemp(TempUnit input)
	{
		return input == TempUnit.Fahrenheit ? FeelsLikeFahrenheitTemp : FeelsLikeCelsiusTemp;
	}

	public float FahrenheitTemp;
	public Vector2 MinMaxFahrenheitTemp;
	public float FeelsLikeFahrenheitTemp;

	public float CelsiusTemp;
	public Vector2 MinMaxCelsiusTemp;
	public float FeelsLikeCelsiusTemp;
}

public enum TempUnit { Fahrenheit, Celsius }

public class GeoManager : MonoBehaviour
{
	[ReadOnly] public Vector3 GeoGPS;
	public List<Sprite> WeatherIcons = new List<Sprite>();
	[ReadOnly]public WeatherData CurrentData;

	TempUnit _TempUnit;

	public TempUnit TempUnit = TempUnit.Fahrenheit;

	string APPID = "6723538e885100078e1e5cecf921af83";

	Coroutine GPSTask;

	private void Start()
	{
		UpdateWeatherData();
	}

	public void UpdateWeatherData()
	{
		if (!Input.location.isEnabledByUser)
		{
			//load test one
			TestWeather();
		}
		else
		{
			if (GPSTask != null) StopCoroutine(GPSTask);
			GPSTask = StartCoroutine(GPSIEnumerator());
		}	
	}

	IEnumerator GPSIEnumerator()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start();

		// Wait until service initializes
		int maxWait = 10;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			yield break;
		}
		else
		{
			// Access granted and location value could be retrieved
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
			GeoGPS = new Vector3(Input.location.lastData.latitude, Input.location.lastData.longitude, Input.location.lastData.altitude);
			yield return StartCoroutine(GetGPSWeatherIEnumerator(GeoGPS));
		}

		// Stop service if there is no need to query location updates continuously
		Input.location.Stop();
	}

	public Vector3 TestInfo;
	Coroutine TestWeatherRoutine;

	[Button]
	public void TestWeather()
	{
		if (TestWeatherRoutine != null) StopCoroutine(TestWeatherRoutine);
		TestWeatherRoutine = StartCoroutine(GetGPSWeatherIEnumerator(TestInfo));
	}

	IEnumerator GetGPSWeatherIEnumerator(Vector3 gps)
	{
		string url = $"api.openweathermap.org/data/2.5/weather?lat={gps.x}&lon={gps.y}&appid={APPID}";
		//http://api.openweathermap.org/data/2.5/weather?lat=37.2296566&lon=-80.4136767&appid=6723538e885100078e1e5cecf921af83
		//Debug.Log(url);

		UnityWebRequest www = UnityWebRequest.Get(url);
		yield return www.SendWebRequest();

		if (www.error == null)
		{
			string CallbackText = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

			JSONNode data = JSON.Parse(CallbackText);

			CurrentData = new WeatherData();
			CurrentData.WeatherCity = data["name"].Value.ToTitleCase();
			CurrentData.WeatherIcon = GetWeatherIcon(data["weather"].AsArray[0]["icon"].Value);
			CurrentData.WeatherInfo = data["weather"].AsArray[0]["description"].Value.ToTitleCase();
			CurrentData.Coordinate = new Vector2(data["coord"]["lat"].AsFloat, data["coord"]["lon"].AsFloat);
			CurrentData.Country = data["sys"]["country"].Value;

			CurrentData.Humidity = data["main"]["humidity"].AsFloat;


			CurrentData.FahrenheitTemp =K2F(data["main"]["temp"].AsFloat);
			CurrentData.MinMaxFahrenheitTemp = new Vector2(K2F(data["main"]["temp_min"].AsFloat), K2F(data["main"]["temp_max"].AsFloat));
			CurrentData.FeelsLikeFahrenheitTemp = K2F(data["main"]["feels_like"].AsFloat);

			CurrentData.CelsiusTemp = K2C(data["main"]["temp"].AsFloat);
			CurrentData.MinMaxCelsiusTemp = new Vector2(K2C(data["main"]["temp_min"].AsFloat), K2C(data["main"]["temp_max"].AsFloat));
			CurrentData.FeelsLikeCelsiusTemp = K2C(data["main"]["feels_like"].AsFloat);
		}
		else
		{
			Debug.Log("ERROR: " + www.error);
		}
	}

	Sprite GetWeatherIcon(string name)
	{
		return WeatherIcons.First(x => x.name.ToLower().Contains(name.ToLower()));
	}

	float K2F(float input)
	{
		return (input - 273.15f) * 9f / 5f + 32f;
	}

	float K2C(float input)
	{
		return input -273.15f;
	}
}
