﻿using com.ootii.Messages;
using UnityEngine;
using UnityEngine.Networking;

public class PageManager : MonoBehaviour
{

	[Header("Pages")]
	public GameObject MainPage;
	public GameObject ChatPage;

	private void Awake()
	{
		Application.targetFrameRate = Screen.currentResolution.refreshRate;
		//Debug.Log(Screen.currentResolution.refreshRate);
	}

	private void Start()
	{
		this.GetComponent<RectTransform>().localPosition = Vector2.zero;
		foreach (Transform t in transform)
		{
			if (t == this.transform) continue;
			t.GetComponent<RectTransform>().localPosition = Vector2.zero;
			t.gameObject.SetActive(false);
		}

		MainPage.gameObject.SetActive(true);
	}

	public void TakePhoto()
	{
		Manager.CameraManager.TakePhoto();
	}
	public void OpenPhotoViwerPanel()
	{
		MessageDispatcher.SendMessage("OpenPhotoViwerPanel");
	}

	public void OpenWeatherPanel()
	{
		MessageDispatcher.SendMessage("OpenWeatherPanel");
	}

	public void DialNumber()
	{
		Application.OpenURL("tel:5409988806");
	}

	public void OpenChatPage()
	{
		ChatPage.SetActive(true);
	}

	public void SendText()
	{
		string mobile_num = "5409988806";
		string message = "This is a test from Unity";

		//#if UNITY_ANDROID
		//            string URL = string.Format("sms:{0}?body={1}",mobile_num,message);
		//#endif

		//#if UNITY_IOS
		string URL = string.Format("sms:{0}?&body={1}", mobile_num, UnityWebRequest.EscapeURL(message));
		//#endif

		//Execute Text Message
		Application.OpenURL(URL);
	}

	public void SOS()
	{
		Application.OpenURL("tel:110");
	}

 
}
