﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
	public int MaxPhotoResolution = 99999;
	public void TakePhoto()
	{
		// Don't attempt to use the camera if it is already open
		if (NativeCamera.IsCameraBusy())
			return;

		// Take a picture with the camera
		// If the captured image's width and/or height is greater than 512px, down-scale it
		NativeCamTakePicture(MaxPhotoResolution);
	}

	public void RecordVideo()
	{
		// Don't attempt to use the camera if it is already open
		if (NativeCamera.IsCameraBusy())
			return;
		// Record a video with the camera
		NativeCamRecordVideo();
	}


	void NativeCamTakePicture(int maxSize)
	{
		NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				// Create a Texture2D from the captured image
				Texture2D texture = NativeCamera.LoadImageAtPath(path, maxSize);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}

				MessageDispatcher.SendMessage(this.gameObject, "OpenPhotoViwerPanelAndLoad", texture, 0);

				//// Assign texture to a temporary quad and destroy it after 5 seconds
				//GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
				//quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
				//quad.transform.forward = Camera.main.transform.forward;
				//quad.transform.localScale = new Vector3(1f, texture.height / (float)texture.width, 1f);

				//Material material = quad.GetComponent<Renderer>().material;
				//if (!material.shader.isSupported) // happens when Standard shader is not included in the build
				//	material.shader = Shader.Find("Legacy Shaders/Diffuse");

				//material.mainTexture = texture;

				//Destroy(quad, 5f);

				//// If a procedural texture is not destroyed manually, 
				//// it will only be freed after a scene change
				//Destroy(texture, 5f);
			}
		}, maxSize);

		Debug.Log("Permission result: " + permission);
	}

	void NativeCamRecordVideo()
	{
		NativeCamera.Permission permission = NativeCamera.RecordVideo((path) =>
		{
			Debug.Log("Video path: " + path);
			if (path != null)
			{
				// Play the recorded video
				Handheld.PlayFullScreenMovie("file://" + path);
			}
		});

		Debug.Log("Permission result: " + permission);
	}
}
