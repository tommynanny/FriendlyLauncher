﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour
{
	public PhotoViwerPanel PhotoViwerPanel;
	public WeatherPanel WeatherPanel;
	private void Awake()
	{
		//register Event
		MessageDispatcher.AddListener("OpenPhotoViwerPanel", 
			delegate {
				PhotoViwerPanel.gameObject.SetActive(true);
			}, true);

		MessageDispatcher.AddListener("OpenPhotoViwerPanelAndLoad",
			msg => {
				PhotoViwerPanel.LoadPhoto((Texture)msg.Data);
				PhotoViwerPanel.gameObject.SetActive(true);
			}, true);

		MessageDispatcher.AddListener("OpenWeatherPanel",
		msg => {
			WeatherPanel.gameObject.SetActive(true);
		}, true);
	}

	private void Start()
	{
		this.GetComponent<RectTransform>().localPosition = Vector2.zero;
		foreach (Transform t in transform)
		{
			if (t == this.transform) continue;
			t.GetComponent<RectTransform>().localPosition = Vector2.zero;
			t.gameObject.SetActive(false);
		}
	}



}
