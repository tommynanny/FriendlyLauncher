﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SafeAreaManager : MonoBehaviour
{
	[ReadOnly] public float ScreenHeight;
	[ReadOnly] public float SafeAreaHeight;
	[ReadOnly] public float OffsetToTop;
	[ReadOnly] public float OffsetToBottom;

	public bool UseBottomSafeArea;
	public bool UseTopSafeArea;

	RectTransform canvasRect;
	RectTransform thisRect;
	private void Update()
	{
		Recalculate();
	}

	void Recalculate()
	{
		if (canvasRect == null) canvasRect = (RectTransform)GetComponentInParent<Canvas>().transform;
		if (thisRect == null) thisRect = (RectTransform)transform;

		ScreenHeight = canvasRect.rect.height;
		SafeAreaHeight = thisRect.rect.height;
		float halfHeight = (ScreenHeight - SafeAreaHeight) / 2;
		OffsetToTop = halfHeight + Mathf.Abs(thisRect.localPosition.y);
		OffsetToBottom = halfHeight - Mathf.Abs(thisRect.localPosition.y);
	}

	public void StretchFullScreen(RectTransform rect)
	{
		Recalculate();
		rect.SetTop(0f);
		rect.SetBottom(0f);
	}

	public void StretchSafeArea(RectTransform rect)
	{
		Recalculate();

		rect.SetTop(this.UseTopSafeArea ? OffsetToTop : 0f);
		rect.SetBottom(this.UseBottomSafeArea ? OffsetToBottom : 0f);


	}
}
