﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Manager : MonoBehaviour
{
	public static Manager instance;
	public static SafeAreaManager SafeAreaManager;
	public static CameraManager CameraManager;
	public static PanelManager PanelManager;
	public static PageManager PageManager;
	public static GeoManager GeoManager;
	//name all manager variable(instance) same name as its type(class) for
	//the reflection to find it (and place it as a component on the child 
	//empty object with same name as well)
	 
	#region Setup
	private void Awake()
	{
		Initialize();
	}

	void Initialize()
	{
		var propertyInfos = this.GetType().GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Static);
		foreach (var propertyInfo in propertyInfos)
		{
			var thisValue = propertyInfo.GetValue(null);
			if (thisValue == null)
			{
				if (propertyInfo.Name != propertyInfo.FieldType.Name)
				{
					//this is instance;
					propertyInfo.SetValue(null, this);
				}
				else
				{
					//Debug.Log(propertyInfo.Name);
					propertyInfo.SetValue(null, GameObject.FindObjectOfType(propertyInfo.FieldType));
					//propertyInfo.SetValue(null, this.transform.Find(propertyInfo.Name).GetComponent(propertyInfo.FieldType));
				}
			}
		}
	}
	#endregion
	//
}