﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct TransformData
{
	public Vector3 Position;
	public Vector3 EulerAngles;
	public TransformData(Vector3 pos, Vector3 euler)
	{
		this.Position = pos;
		this.EulerAngles = euler;
	}
}

public static class ExtensionMethods
{
	public static List<int> AllIndexesOf(this string str, string value)
	{
		if (String.IsNullOrEmpty(value))
			throw new ArgumentException("the string to find may not be empty", "value");
		List<int> indexes = new List<int>();
		for (int index = 0; ; index += value.Length)
		{
			index = str.IndexOf(value, index);
			if (index == -1)
				return indexes;
			indexes.Add(index);
		}
	}

	#region Transform Position
	public static void SetPositionX(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.x = input;
		tran.position = temp;
	}

	public static void SetPositionY(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.y = input;
		tran.position = temp;
	}
	public static void SetPositionZ(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.z = input;
		tran.position = temp;
	}

	public static void SetLocalPositionX(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.x = input;
		tran.localPosition = temp;
	}

	public static void SetLocalPositionY(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.y = input;
		tran.localPosition = temp;
	}
	public static void SetLocalPositionZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.z = input;
		tran.localPosition = temp;
	}
	#endregion

	#region Transform Rotation
	public static void SetEulerAnglesX(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.x = input;
		tran.eulerAngles = temp;
	}

	public static void SetEulerAnglesY(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.y = input;
		tran.eulerAngles = temp;
	}

	public static void SetEulerAnglesZ(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.z = input;
		tran.eulerAngles = temp;
	}

	public static void SetLocalEulerAnglesX(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.x = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalEulerAnglesY(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.y = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalEulerAnglesZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.z = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalScaleX(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.x = input;
		tran.localScale = temp;
	}

	public static void SetLocalScaleY(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.y = input;
		tran.localScale = temp;
	}

	public static void SetLocalScaleZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.z = input;
		tran.localScale = temp;
	}

	#endregion

	#region Vec3
	public static Vector3 SetX(this Vector3 vec, float x)
	{
		return new Vector3(x, vec.y, vec.z);
	}

	public static Vector3 SetY(this Vector3 vec, float y)
	{
		return new Vector3(vec.x, y, vec.z);
	}

	public static Vector3 SetZ(this Vector3 vec, float z)
	{
		return new Vector3(vec.x, vec.y, z);
	}
	#endregion

	#region String
	public static string ToTitleCase(this string title)
	{
		return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
	}
	#endregion
	
	public static Vector3 MouseWorldPosition(Camera cam)
	{
		return cam.ScreenToWorldPoint(Input.mousePosition.SetZ(-cam.transform.position.z)).SetZ(0f);
	}

	public static bool ContainsLayer(this LayerMask layermask, int layer)
	{
		return layermask == (layermask | (1 << layer));
	}

	public static void Each<T>(this IEnumerable<T> items, Action<T> action)
	{
		foreach (var item in items)
			action(item);
	}

	public static IEnumerable<T> GetAddedFromMeTo<T>(this IEnumerable<T> listA, IEnumerable<T> listB)
	{
		return listB.Except(listA).ToList();
	}

	public static IEnumerable<T> GetRemovedFromMeTo<T>(this IEnumerable<T> listA, IEnumerable<T> listB)
	{
		return listA.Except(listB).ToList();
	}

	public static bool HasSameGameObjectWith(this IEnumerable<Component> listA, IEnumerable<Component> listB)
	{
		if (listA.Count() != listB.Count()) return false;
		int sum = 0;
		foreach (var i in listA)
		{
			foreach (var j in listB)
			{
				if (i.gameObject.GetHashCode() == j.gameObject.GetHashCode())
				{
					sum++;
				}
			}
		}

		return sum == listA.Count();
	}

	#region Transform
	//Breadth-first search
	public static Transform FindDeepChild(this Transform aParent, string aName)
	{
		Queue<Transform> queue = new Queue<Transform>();
		queue.Enqueue(aParent);
		while (queue.Count > 0)
		{
			var c = queue.Dequeue();
			if (c.name == aName)
				return c;
			foreach (Transform t in c)
				queue.Enqueue(t);
		}
		return null;
	}

	public static void Load(this Transform tran, TransformData data, bool WorldSpace = true)
	{
		if (WorldSpace)
		{
			tran.position = data.Position;
			tran.eulerAngles = data.EulerAngles;
		}
		else
		{
			tran.localPosition = data.Position;
			tran.localEulerAngles = data.EulerAngles;
		}

	}
	public static TransformData Save(this Transform tran, bool WorldSpace = true)
	{
		return WorldSpace ? new TransformData(tran.position, tran.eulerAngles) : new TransformData(tran.localPosition, tran.localEulerAngles);
	}
	#endregion
	#region rectTransform

	public static void StretchParent(this RectTransform rt)
	{
		rt.anchorMin = Vector2.zero;
		rt.anchorMax = new Vector2(1, 1);
		rt.SetLeftRightTopBottom(0, 0, 0, 0);
	}

	public static void SetLeftRightTopBottom(this RectTransform rt, float left, float right, float top, float bottom)
	{
		rt.SetLeft(left);
		rt.SetRight(right);
		rt.SetTop(top);
		rt.SetBottom(bottom);
	}
	public static void SetLeft(this RectTransform rt, float left)
	{
		rt.offsetMin = new Vector2(left, rt.offsetMin.y);
	}

	public static void SetRight(this RectTransform rt, float right)
	{
		rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
	}

	public static void SetTop(this RectTransform rt, float top)
	{
		rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
	}

	public static void SetBottom(this RectTransform rt, float bottom)
	{
		rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
	}
	#endregion
}
