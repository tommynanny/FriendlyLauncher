﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class UIDebug : MonoBehaviour
{
	[ReadOnly]
	public Vector2 DeltaSize;
	[ReadOnly]
	public Vector2 RectWH;
	[ReadOnly]
	public Vector3 RectAnchorPosition;
	[ReadOnly]
	public Vector3 RectLocalPosition;

	RectTransform rectTrans;

	private void OnEnable()
	{
		rectTrans = this.GetComponent<RectTransform>();
	}

	private void Update()
	{
		DeltaSize = new Vector2(rectTrans.sizeDelta.x, rectTrans.sizeDelta.y);
		RectWH = new Vector2(rectTrans.rect.width, rectTrans.rect.height);
		RectAnchorPosition = rectTrans.anchoredPosition;
		RectLocalPosition = rectTrans.localPosition;
	}
}
