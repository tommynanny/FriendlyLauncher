﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SafeAreaMod { FullScreen, SafeArea }

public class SafeAreaController : MonoBehaviour
{
	public SafeAreaMod SafeAreaMod;
	RectTransform thisRect;

	private void Update()
	{
		if (thisRect == null) thisRect = (RectTransform)transform;
		if (SafeAreaMod == SafeAreaMod.FullScreen)
		{
			Manager.SafeAreaManager.StretchFullScreen(thisRect);
		}
		else if (SafeAreaMod == SafeAreaMod.SafeArea)
		{
			Manager.SafeAreaManager.StretchSafeArea(thisRect);
		}
	}
}
