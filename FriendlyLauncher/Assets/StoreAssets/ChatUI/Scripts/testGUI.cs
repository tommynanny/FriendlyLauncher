﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testGUI : MonoBehaviour {
    public ChatPage ui;

    string text = "";

	//// Use this for initialization
	//void Start () {
		
	//}
	
	//// Update is called once per frame
	//void Update () {
		
	//}

    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 150, 35), "Add Time Tag"))
        {
            ui.AddDateTimeTag();
        }
        if (GUI.Button(new Rect(0, 60, 150, 35), "Left Short Msg"))
        {
            ui.AddChatMessage("Hello", ChatPage.enumChatMessageType.MessageLeft);
        }

        if (GUI.Button(new Rect(0, 100, 150, 35), "Left Middle Msg"))
        {
            ui.AddChatMessage("ABCDEFGHIJKLMNOPQRSTUVWXYZ!!", ChatPage.enumChatMessageType.MessageLeft);
        }

        if (GUI.Button(new Rect(0, 140, 150, 35), "Left Long Msg"))
        {
            ui.AddChatMessage("ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!", ChatPage.enumChatMessageType.MessageLeft);
        }
        if (GUI.Button(new Rect(0, 180, 150, 35), "Left Chinese Msg"))
        {
            ui.AddChatMessage("湖水这样沉静，这样蓝，一朵洁白的花闪在秋光里很阴暗；早晨，一个少女来湖边叹气，十六岁的影子比红宝石美丽。青海省城有一个郡王，可怕的欲念", ChatPage.enumChatMessageType.MessageLeft);
        }


        if (GUI.Button(new Rect(0, 240, 150, 35), "Right Short Msg"))
        {
            ui.AddChatMessage("Hello", ChatPage.enumChatMessageType.MessageRight);
        }

        if (GUI.Button(new Rect(0, 280, 150, 35), "Right Middle Msg"))
        {
            ui.AddChatMessage("ABCDEFGHIJKLMNOPQRSTUVWXYZ!!", ChatPage.enumChatMessageType.MessageRight);
        }

        if (GUI.Button(new Rect(0, 320, 150, 35), "Right Long Msg"))
        {
            ui.AddChatMessage("ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!ABCDEFGHIJKLMNOPQRSTUVWXYZ!", ChatPage.enumChatMessageType.MessageRight);
        }
        if (GUI.Button(new Rect(0, 360, 150, 35), "Right Chinese Msg"))
        {
            ui.AddChatMessage("湖水这样沉静，这样蓝，一朵洁白的花闪在秋光里很阴暗；早晨，一个少女来湖边叹气，十六岁的影子比红宝石美丽。青海省城有一个郡王，可怕的欲念", ChatPage.enumChatMessageType.MessageRight);
        }


        if (GUI.Button(new Rect(Screen.width - 200,0,150,35),"Clear messages"))
        {
            ui.ClearChatItems();
        }
        if (GUI.Button(new Rect(Screen.width - 200,40,150,35),"Get all items"))
        {
            text = "";
            List<ChatItem> items = ui.GetChatItems();
            foreach(ChatItem item in items)
            {
                if (item.itemType!= ChatItem.enumChatItemType.DateTimeTag)
                {
                    text += item.time.ToShortTimeString() + item.text + "\n\r";
                }
            }
        }

        if (text!="")
        {
            GUI.Label(new Rect(Screen.width - 500, 100, 500, 1000), text);
        }
    }
}
