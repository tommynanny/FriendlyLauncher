﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testAutoReply : MonoBehaviour {
    public ChatPage ui;
    // Use this for initialization

	void Start () {
        if (ui!=null)
            ui.OnChatMessage += Ui_OnChatMessage;
	}

    private void Ui_OnChatMessage(string text, ChatPage.enumChatMessageType messageType)
    {
        if (messageType == ChatPage.enumChatMessageType.MessageRight)
        {
            MessageDispatcher.SendMessage(this, "SendToChatBot", text, 0);
        }
    }

	private void Awake()
	{
        MessageDispatcher.AddListener("ChatBotReply", 
        msg=>{
            string reply = (string)msg.Data;
            ui.AddChatMessage(reply, ChatPage.enumChatMessageType.MessageLeft);
        },
        true);
	}

}
