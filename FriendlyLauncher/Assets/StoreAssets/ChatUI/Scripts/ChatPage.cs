﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatPage : MonoBehaviour
{
	public void ClosePage()
	{
		this.gameObject.SetActive(false);
	}
	 
	public enum enumChatMessageType
	{
		MessageLeft,
		MessageRight,
	}
	//public float chatInputHeight = 180f;

	RectTransform rootTransform;

	[Header("Controls")]
	public RectTransform contentTransform;
	public InputField inputField;

	[Header("Sprite")]
	public Sprite leftFaceSprite;
	public Sprite rightFaceSprite;

	[Header("MessageBox")]
	public GameObject dateTimeTag;
	public GameObject chatLeftMessageBox;
	public AudioClip chatLeftMessageSound;

	public GameObject chatRightMessageBox;
	public AudioClip chatRightMessageSound;

	public float contentHeight = 0;

	Text prevText;
	AudioSource audioSource;
	List<ChatItem> chatItems = new List<ChatItem>();
	DateTime timeTag;

	//float MESSAGE_ICONS_WIDTH = 65;
	//float MESSAGE_MIN_WIDTH = 130;
	//float MESSAGE_MAX_WIDTH = 300;
	float MESSAGE_SINGLE_LINE_WIDTH;

	float MESSAGE_RIGHT_MAGIN = 10;

	public delegate void chatMessage(string text, enumChatMessageType messageType);
	public event chatMessage OnChatMessage;

	// Use this for initialization
	void Start()
	{

		timeTag = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

		rootTransform = GetComponent<RectTransform>();

		//float w = rootTransform.offsetMax.x - rootTransform.offsetMin.x;
		float w = rootTransform.rect.width;

		MESSAGE_SINGLE_LINE_WIDTH = w / 5 * 3;


		if (inputField != null)
		{
			inputField.onEndEdit.AddListener(delegate
			{
				if (inputField.text == "")
					return;

				AddChatMessage(inputField.text, enumChatMessageType.MessageRight);
				if (OnChatMessage != null)
					OnChatMessage(inputField.text, enumChatMessageType.MessageRight);
				//AddLeftChatMessageBox(inputField.text);
				inputField.text = "";
				inputField.ActivateInputField();
			});
		}

		if (chatLeftMessageSound != null || chatRightMessageSound != null)
		{
			audioSource = gameObject.AddComponent<AudioSource>();
		}

		InstallContent();
	}


	public void AddDateTimeTag()
	{
		//float h = contentTransform.offsetMax.y - contentTransform.offsetMin.y;
		float h = contentTransform.rect.height;

		string time1 = System.DateTime.Now.Hour.ToString();
		string time2 = System.DateTime.Now.Minute.ToString();

		if (time1.Length == 1) time1 = "0" + time1;
		if (time2.Length == 1) time2 = "0" + time2;

		time1 = time1 + ":" + time2;

		GameObject goTag;

		if (dateTimeTag != null)
		{
			goTag = Instantiate(dateTimeTag);
			goTag.name = "DateTime " + time1;
			RectTransform rectTag = goTag.GetComponent<RectTransform>();
			rectTag.parent = contentTransform;

			//float rectHeight = rectTag.offsetMax.y - rectTag.offsetMin.y;
			float rectHeight = rectTag.rect.height;

			rectTag.localPosition = new Vector3(0, h / 2 - rectHeight / 2 - contentHeight);

			ChatItem item = new ChatItem();
			item.gameObject = goTag;
			item.text = time1;
			item.time = DateTime.Now;
			item.itemType = ChatItem.enumChatItemType.DateTimeTag;

			chatItems.Add(item);

			Transform goChild = rectTag.Find("DateTimeText");
			if (goChild != null)
			{
				Text txt = goChild.GetComponent<Text>();
				if (txt != null)
				{
					txt.text = time1;
				}
			}

			contentHeight += rectHeight;

			CalcContentHeight();
		}
	}

	public void AddChatMessage(string text, enumChatMessageType messageType)
	{
		GameObject templateObject = null;
		switch (messageType)
		{
			case enumChatMessageType.MessageLeft:
				templateObject = chatLeftMessageBox;
				break;
			case enumChatMessageType.MessageRight:
				templateObject = chatRightMessageBox;
				break;
		}

		if (templateObject == null)
			return;

		//判断时间是否超过1分钟?
		TimeSpan ts = DateTime.Now - timeTag;
		if (ts.TotalMinutes >= 1)
		{
			AddDateTimeTag();
			timeTag = DateTime.Now;
		}

		//float w = rootTransform.offsetMax.x - rootTransform.offsetMin.x;
		float w = rootTransform.rect.width;
		//float h = contentTransform.offsetMax.y - contentTransform.offsetMin.y;
		float h = contentTransform.rect.height;

		GameObject goChat;

		if (templateObject != null)
		{
			goChat = Instantiate(templateObject);
			RectTransform rectChat = goChat.GetComponent<RectTransform>();
			rectChat.parent = contentTransform;

			//设置Face
			if (leftFaceSprite != null)
			{
				Transform tranFace = rectChat.Find("Face/FaceImage");
				if (tranFace != null)
				{
					Image imgFace = tranFace.GetComponent<Image>();
					if (imgFace != null)
					{
						if (messageType == enumChatMessageType.MessageLeft)
							imgFace.sprite = leftFaceSprite;
						else
							imgFace.sprite = rightFaceSprite;
					}
				}
			}



			//设置文字
			RectTransform tranText = (RectTransform)rectChat.Find("MessageBox/MessageText");
			if (tranText != null)
			{
				////计算文字的大小
				//TextGenerationSettings settings;
				//TextGenerator tg;
				//tg = txt.cachedTextGeneratorForLayout;
				//settings = txt.GetGenerationSettings(Vector2.zero);

				//Canvas.ForceUpdateCanvases();
				//float ActualTxtWidth = tg.GetPreferredWidth(txt.text, settings) / txt.pixelsPerUnit;
				//float ActualTxtHidth = tg.GetPreferredHeight(txt.text.Substring(0, 1), settings) / txt.pixelsPerUnit;
				rectChat.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, MESSAGE_SINGLE_LINE_WIDTH + 305);
				rectChat.anchoredPosition -= new Vector2((MESSAGE_SINGLE_LINE_WIDTH + 305 - rectChat.rect.width) / 2, 0);

				Canvas.ForceUpdateCanvases();

				Text txt = tranText.GetComponent<Text>();
				txt.text = text;
				prevText = txt;

				Canvas.ForceUpdateCanvases();

				float txtWidth = tranText.rect.width;
				float txtHeight = tranText.rect.height;

				//if (txtWidth + MESSAGE_ICONS_WIDTH < MESSAGE_MIN_WIDTH)
				//{
				//    //设置自己为最小宽度
				//    rectChat.offsetMax = new Vector2(rectChat.offsetMin.x + MESSAGE_MIN_WIDTH, rectChat.offsetMax.y);
				//}

				Vector2 PreferredSize = new Vector2(txt.preferredWidth, txt.preferredHeight);

				if (PreferredSize.x > txtWidth)
				{
					rectChat.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, MESSAGE_SINGLE_LINE_WIDTH + 305);
				}
				else
				{
					rectChat.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, PreferredSize.x + 305);
				}

				if (PreferredSize.y > txtHeight)
				{
					rectChat.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, PreferredSize.y + 155);
				}
				else
				{
					rectChat.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, txtHeight + 155);
				}


				//if (txtWidth + MESSAGE_ICONS_WIDTH > MESSAGE_MIN_WIDTH && txtWidth<MESSAGE_MAX_WIDTH)
				//{
				//    //单行，设置宽度
				//    float wth = txtWidth + 80f;
				//    if (messageType == enumChatMessageType.MessageRight) wth += MESSAGE_RIGHT_MAGIN;
				//    rectChat.offsetMax = new Vector2(rectChat.offsetMin.x + wth, rectChat.offsetMax.y);
				//}

				//if (txtWidth + MESSAGE_ICONS_WIDTH > MESSAGE_MAX_WIDTH)
				//{
				//    //多行，设置最宽的宽度

				//    int lineHeight = (int)((txtWidth  )/ MESSAGE_MAX_WIDTH) - 1;
				//    if (lineHeight < 1) lineHeight = 1;

				//    float hh = txtHeight * lineHeight;
				//    rectChat.offsetMax = new Vector2(rectChat.offsetMin.x + MESSAGE_MAX_WIDTH, rectChat.offsetMax.y + hh);

				//}

			}


			//float rectWidth = rectChat.offsetMax.x - rectChat.offsetMin.x;
			float rectWidth = rectChat.rect.width;

			//float rectHeight = rectChat.offsetMax.y - rectChat.offsetMin.y;
			float rectHeight = rectChat.rect.height;

			if (messageType == enumChatMessageType.MessageLeft)
			{
				//向左对齐
				rectChat.localPosition = new Vector3(rectWidth / 2 - w / 2, h / 2 - rectHeight / 2 - contentHeight);
			}
			else
			{
				//向右对齐
				rectChat.localPosition = new Vector3(w / 2 - rectWidth / 2 - MESSAGE_RIGHT_MAGIN, h / 2 - rectHeight / 2 - contentHeight);

			}

			ChatItem item = new ChatItem();
			item.gameObject = goChat;
			item.text = text;
			item.time = DateTime.Now;
			if (messageType == enumChatMessageType.MessageLeft)
				item.itemType = ChatItem.enumChatItemType.LeftChatMessage;
			else
				item.itemType = ChatItem.enumChatItemType.RightChatMessage;

			chatItems.Add(item);


			contentHeight += rectHeight;
			CalcContentHeight();
		}

		PlaySound(messageType);
	}

	void PlaySound(enumChatMessageType messageType)
	{
		if (audioSource == null)
			return;

		AudioClip clip = null;

		switch (messageType)
		{
			case enumChatMessageType.MessageLeft:
				clip = chatLeftMessageSound;
				break;
			case enumChatMessageType.MessageRight:
				clip = chatRightMessageSound;
				break;
		}

		if (clip == null)
			return;

		audioSource.clip = clip;
		audioSource.Play();
	}

	void InstallContent()
	{
		////float h = rootTransform.offsetMax.y - rootTransform.offsetMin.y;
		//float h = rootTransform.rect.height;

		//RectTransform rect = (RectTransform)contentTransform.parent.parent;
		////float hp = Mathf.Abs(rect.offsetMax.y - rect.offsetMin.y);
		//float hp = Mathf.Abs(rect.rect.height);

		//h = h - hp;

		//contentTransform.offsetMin = new Vector2(0, 0);
		//contentTransform.offsetMax = new Vector2(0, h);
		//contentTransform.localPosition = new Vector3(0, 0);

	}

	//重新计算Content的高度并移动到最底部
	void CalcContentHeight()
	{
		float ParentHeight = ((RectTransform)contentTransform.parent).rect.height;
		if (contentHeight <= ParentHeight)
		{
			return;
		}
			

		contentTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contentHeight);
		contentTransform.anchoredPosition = new Vector2(0, contentTransform.sizeDelta.y / 2);

	}

	public void ClearChatItems()
	{
		foreach (ChatItem item in chatItems)
		{
			Destroy(item.gameObject);
		}
		chatItems.Clear();

		timeTag = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
		contentHeight = 0;

		ResetChatRect();

		InstallContent();
	}

	void ResetChatRect()
	{
		contentTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, ((RectTransform)contentTransform.parent).rect.height);
		contentTransform.anchoredPosition = new Vector2(0, 0);
	}

	public ChatItem GetChatItem(int index)
	{
		if (index < 0) return null;
		if (index >= chatItems.Count) return null;

		return chatItems[index];
	}

	public List<ChatItem> GetChatItems()
	{
		return chatItems;
	}


}
